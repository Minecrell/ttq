#!/usr/bin/python3
# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import ttq.args
import ttq.cache
import ttq.match
import ttq.print
import ttq.wiki


def match_and_print():
    query = ttq.args.ARGS.QUERY

    # Match
    if query and query.startswith("ui:"):
        ttq.match.by_ui()
    else:
        ttq.match.by_device()
    ttq.match.notes()

    # Print
    if query and query.startswith("ui:"):
        ttq.print.by_ui()
    elif query and query.startswith("soc:"):
        ttq.print.by_soc()
    else:
        ttq.print.by_device()

    if ttq.args.ARGS.notes:
        ttq.print.notes()


def main():
    ttq.args.parse_args()
    ttq.args.parse_args_query()
    ttq.args.parse_args_user_type()

    ttq.wiki.parse_testing_team()
    print("---")
    match_and_print()


main()
