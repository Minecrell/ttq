# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import ttq.match


def format_user_list(users):
    ret = []
    for user in users:
        if ttq.args.ARGS.matrix:
            ret += [user["matrix"]]
        else:
            ret += [f"@{user['gitlab']}"]

    return ", ".join(sorted(ret))


def by_ui():
    for ui in sorted(ttq.match.UIS_MATCHED):
        users = ttq.match.UIS_MATCHED[ui]
        ui_short = ui.split("postmarketos-ui-", 1)[1]
        print(f"* {ui_short} ({format_user_list(users)})")

    print()
    print(f"Matched {len(ttq.match.UIS_MATCHED)} user interfaces,"
          f" {len(ttq.match.USERS_MATCHED)} {ttq.args.ARGS_USER_TYPE} users.")


def by_device():
    for device in sorted(ttq.match.DEVICES_MATCHED):
        users = ttq.match.DEVICES_MATCHED[device]
        print(f"* {device} ({format_user_list(users)})")

    print()
    print(f"Matched {len(ttq.match.DEVICES_MATCHED)} devices,"
          f" {len(ttq.match.USERS_MATCHED)} {ttq.args.ARGS_USER_TYPE} users.")


def by_soc():
    for soc in ttq.match.SOCS_MATCHED:
        print(f"* {soc}:")
        for device in sorted(ttq.match.SOCS_MATCHED[soc]):
            users = ttq.match.DEVICES_MATCHED[device]
            print(f"  * {device} ({format_user_list(users)})")

    print()
    print(f"Matched {len(ttq.match.SOCS_MATCHED)} SoCs,"
          f" {len(ttq.match.DEVICES_MATCHED)} devices,"
          f" {len(ttq.match.USERS_MATCHED)} {ttq.args.ARGS_USER_TYPE} users.")

def notes():
    notes = ttq.match.NOTES_MATCHED
    if not notes:
        return

    print()
    for user, text in notes.items():
        print(f"{user}:")
        print(f"  {text}")
