# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import urllib.request
import os
import time
import shutil

import ttq.args

CACHE_DIR = f"{os.environ.get('HOME')}/.cache/ttq"
CACHE_VALID_SEC = 3600


def cache_path(name):
    return f"{CACHE_DIR}/{name}"


def cache_is_outdated(name):
    path = cache_path(name)
    if not os.path.exists(path) or ttq.args.ARGS.force_update:
        return True
    if ttq.args.ARGS.offline:
        return False
    return os.path.getmtime(path) + CACHE_VALID_SEC < time.time()


def cache_update(name, url):
    path = cache_path(name)
    if not os.path.exists(path):
        os.makedirs(CACHE_DIR, exist_ok=True)

    if cache_is_outdated(name):
        print(f"Updating {name} ({url})")
        with urllib.request.urlopen(url) as response:
            with open(path, "wb") as handle:
                shutil.copyfileobj(response, handle)

    return path
