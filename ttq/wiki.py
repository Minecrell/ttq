# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
import re
import ttq.cache
import ttq.html
import urllib.parse

WIKI_URL = "https://wiki.postmarketos.org/wiki"
TESTING_TEAM = []
SOCS = []

def parse_testing_team():
    global TESTING_TEAM
    cache_name = "testing_team.html"

    ttq.cache.cache_update(cache_name, f"{WIKI_URL}/Testing_Team")
    start = '<h2><span class="mw-headline" id="Members">Members</span></h2>'
    end = '<h2><span class="mw-headline" id="See_also">See also</span></h2>'

    for tds in ttq.html.parse_table(cache_name, start, end):
        TESTING_TEAM += [{"wiki": tds[1],
                          "gitlab": tds[2],
                          "matrix": tds[3],
                          "devices": tds[4].split(" "),
                          "uis": tds[5].split(" "),
                          "notes": tds[6]}]


def parse_socs():
    global SOCS
    cache_name = "category_socs.html"

    ttq.cache.cache_update(cache_name, f"{WIKI_URL}/Category:Socs")
    start = '<div class="mw-category-group">'
    end = '<div class="printfooter">'

    html = ttq.html.read_from_cache(cache_name, start, end)
    SOCS = ttq.html.parse_list(html)


def parse_soc_devices(soc):
    cache_name = f"soc_{re.sub('[^A-Za-z0-9]+', '', soc)}.html".lower()

    ttq.cache.cache_update(cache_name, f"{WIKI_URL}/{urllib.parse.quote(soc)}")
    html = ttq.html.read_from_cache(cache_name)
    tds = html.split('<td class="field_Codename">')

    ret = []
    for i in range(1, len(tds)):
        td = tds[i].split("</td>")[0]
        td = ttq.html.strip_html(td)
        td = td.replace(","," ")
        for device in td.split(" "):
            if not device:
                continue
            ret += [f"={device}="]

    if not ret:
        print(f"ERROR: couldn't find any devices in SoC page: {cache_name}")

    return ret
