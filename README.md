# ttq

testing team query

CLI program to query the [testing team](https://wiki.postmarketos.org/wiki/Testing_Team)
by SoC, device and UI.

```
$ ./ttq.py -h
usage: ttq.py [-h] [-o] [-f] [-m] [-n] [QUERY]

examples:
  list users by device:
  $ ./ttq.py  # all
  $ ./ttq.py librem  # matching *librem*
  $ ./ttq.py pinephone=  # matching *pinephone (not the pro)

  list users by SoC/device:
  $ ./ttq.py soc:sdm845  # matching *sdm845*

  list users by UI:
  $ ./ttq.py ui:  # all
  $ ./ttq.py ui:sxmo  # matching *sxmo*

positional arguments:
  QUERY               what to search for

options:
  -h, --help          show this help message and exit
  -o, --offline       never treat cached files as outdated
  -f, --force-update  force update of cached files
  -m, --matrix        show matrix users instead of gitlab users
  -n, --notes         print notes (if any)
```
